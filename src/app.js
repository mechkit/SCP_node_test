/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/
console.log('/\\');

import 'normalize.css';

import hash_router from 'hash_router';

import mkwebsite from 'mkwebsite';
import update from './update';
import reducers from './reducers';
import init_state from './init_state';

var state = init_state();

var global = window || global;

var actions = mkwebsite(state, reducers, update);

if( !Object.keys(state.db.users).length ){
  console.log('Users empty: populating');
  actions.add_user({
    account_type: 'installer',
    name: 'Jane Doe',
    company: 'Octopus inc.',
  });
  actions.add_user({
    account_type: 'installer',
    name: 'Michael Shellstrop',
    company: 'Bonendroit inc.',
    system_ids: ['6bd60'],
  });
  actions.add_user({
    account_type: 'admin',
    name: 'AJ Nepenthes',
    company: 'FSEC',
  });
}

if( !Object.keys(state.db.users).length ){
  console.log('Users empty: populating');
  actions.add_user({
    account_type: 'installer',
    name: 'Jane Doe',
    company: 'Octopus inc.',
  });
  actions.add_user({
    account_type: 'installer',
    name: 'Michael Shellstrop',
    company: 'Bonendroit inc.',
  });
  actions.add_user({
    account_type: 'admin',
    name: 'AJ Nepenthes',
    company: 'FSEC',
  });
}


// TODO: demo only, non secure
if( state.user_id ){
  actions.login(state.user_id);
}

const default_page = 'default';

var router = hash_router(function(selection){
  if( ! selection ){
    console.log('re-ROUTING... to default');
    selection = [default_page];
  }
  window.scrollTo(0, 0);
  console.log('ROUTING... '+selection.join('/'));

  actions.set_page(selection);
});

router();
