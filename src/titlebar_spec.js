import f from 'functions';

export default function(state){
  var title = state.ui.title;
  var menu = state.ui.menu;
  var selected = state.ui.page_id;

  var selected_page_name = selected.join('/');

  var titlebar_content = {
    tag: 'div',
    props: {
      id: 'titlebar_content'
    },
    children: [
      {
        tag: selected_page_name === 'default' ? 'span' : 'a',
        props: {
          id: 'site_title',
          class: selected_page_name === 'default' ? 'site_title_selected' : '',
          href: state.ui.home_url,
        },
        text: title
      }
    ]
  };

  if( menu && menu.length !== 0 ){

    var menu_specs = {
      tag: 'div',
      props: {
        id: 'menu'
      },
      children: []
    };

    titlebar_content.children.push(menu_specs);

    menu_specs.children.push({
      tag: 'li',
      text: '[',
      props: {
        class: 'menu_bracket',
      }
    });

    menu.forEach(function(name){
      var prety_name = f.pretty_name(name).trim();
      var href = '#/'+name;
      menu_specs.children.push({
        tag: 'li',
        props: {
          class: 'titlebar_option',
        },
        children: [{
          tag: name === selected_page_name ? 'span' : 'a',
          text: prety_name,
          props: {
            class: name === selected_page_name ? 'titlebar_link titlebar_link_selected' : 'titlebar_link',
            href: href
          }
        }]
      });
    });

    menu_specs.children.push({
      tag: 'li',
      text: ']',
      props: {
        class: 'menu_bracket',
      }
    });

  }


  return titlebar_content;
}
