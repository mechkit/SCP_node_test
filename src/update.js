import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
// import base64url from 'base64-url';
import SPD  from './express/SPD';
import mk_page from './mk_page';
import $ from 'specdom';

// var specdom_main = Specdom('#content');

export default function update(state,actions){
  sessionStorage.setItem('db', JSON.stringify(state.db));

  // var system = state.system;
  // var inputs = state.inputs;
  // var system_code = base64url.encode(JSON.stringify(inputs));
  // window.location.hash = '/'+system_code;

  console.log('#UPDATE');
  mk_page(state, actions);

}
