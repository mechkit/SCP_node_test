import mk_system_data from './mk_system_data';

export default function(){
  var express_inputs = sessionStorage.getItem('express_inputs');
  if( express_inputs ){
    express_inputs = JSON.parse(express_inputs);
  } else {
    express_inputs = {};
  }
  var manual_inputs = sessionStorage.getItem('manual_inputs');
  if( manual_inputs ){
    manual_inputs = JSON.parse(manual_inputs);
  } else {
    manual_inputs = {
      module: {},
      inverter: {},
      optimizer: {},
      storage: {},
      interconnection: {},
    };
  }

  var module_inputs = sessionStorage.getItem('module_inputs');
  if( module_inputs ){
    module_inputs = JSON.parse(module_inputs);
  } else {
    module_inputs = {
      type: {},
    };
  }

  var user_id = sessionStorage.getItem('user_id') || false;

  var db = sessionStorage.getItem('db');
  if( db ){
    db = JSON.parse(db);
  } else {
    db = {
      modules: {},
      systems: {
        '6bd60': {
          'user_id': '8544',
          'inputs': {
            'module': {
              'manufacturer_name': 'Canadian Solar Inc.',
              'device_name': 'CS3U-330P',
              'num_of_strings': 2,
              'smallest_string': 8,
              'largest_string': 8,
              'num_of_modules': 16
            },
            'inverter': {
              'manufacturer_name': 'SMA',
              'device_name': 'SB5.0-1SP-US-40'
            },
            'optimizer': {
              'manufacturer_name': 'SolarEdge'
            },
            'interconnection': {
              'bussbar_rating': 225,
              'supply_ocpd_rating': 200,
              'load_breaker_total': 50
            }
          },
          'system': {
            'company': {
              'device_id': null,
              'certification_id': null,
              'old_certification_number': null,
              'company_id': null,
              'name': null,
              'line_1': null,
              'line_2': null,
              'line_3': null,
              'city': null,
              'state': null,
              'zipcode': null,
              'country': null,
              'primary_phone_number': null,
              'web_site': null,
              'email': null
            },
            'inverter': {
              'device_model_number': 'SB5.0-1SP-US-40',
              'device_name': 'SB5.0-1SP-US-40',
              'manufacturer_name': 'SMA',
              'device_type_id': 53,
              'pvsystem_id': 2340,
              'inverter_id': 1280,
              'num_of_inverters': 1,
              'grid_voltage': 240,
              'optimizer_id': 300,
              'device_id': 1280,
              'properties_id': 402,
              'properties_updated_per_dev_id': 9499,
              'user_category_code': 'Employee',
              'nominal_ac_output_power_120': 0,
              'mppt_channels': 3,
              'vmax': 600,
              'vstart': 125,
              'mppt_min': 220,
              'mppt_max': 480,
              'imax_total': 18,
              'imax_channel': 10,
              'voltage_range_min': 100,
              'voltage_range_max': 550,
              'max_ac_ocpd': 0,
              'ul_1741': 0,
              'max_dc_inputpower_120': 0,
              'max_dc_inputpower_208': 5150,
              'max_dc_inputpower_240': 5150,
              'max_dc_inputpower_277': 0,
              'max_dc_inputpower_480': 0,
              'nominal_ac_output_power_208': 5000,
              'nominal_ac_output_power_240': 5000,
              'nominal_ac_output_power_277': 0,
              'nominal_ac_output_power_480': 0,
              'max_ac_output_current_120': 0,
              'max_ac_output_current_208': 24,
              'max_ac_output_current_240': 24,
              'max_ac_output_current_277': 0,
              'max_ac_output_current_480': 0,
              'min_panel_wattage': 0,
              'max_panel_wattage': 0,
              'max_module_cells': 0,
              'min_unitsperbranch': 0,
              'max_unitsperbranch': 0,
              'max_watts_per_string': 0,
              'system_type': 'string',
              'opti_id': null,
              'opti_manufacturer_name': null,
              'opti_device_model_number': null,
              'opti_device_name': null,
              'opti_name': null,
              'opti_rated_max_power': null,
              'opti_max_input_voltage': null,
              'opti_mppt_op_range_min': null,
              'opti_mppt_op_range_max': null,
              'opti_max_isc': null,
              'opti_max_output_current': null,
              'opti_max_output_voltage': null,
              'opti_min_optis_per_string': null,
              'opti_max_optis_per_string': null,
              'opti_max_power_per_string': null
            },
            'optimizer': {
              'id': null,
              'manufacturer_name': 'SolarEdge',
              'device_model_number': null,
              'device_name': null,
              'name': null,
              'rated_max_power': null,
              'max_input_voltage': null,
              'mppt_op_range_min': null,
              'mppt_op_range_max': null,
              'max_isc': null,
              'max_output_current': null,
              'max_output_voltage': null,
              'min_optis_per_string': null,
              'max_optis_per_string': null,
              'max_power_per_string': null
            },
            'module': {
              'device_model_number': 'CS3U-330P',
              'device_name': 'CS3U-330P',
              'manufacturer_name': 'Canadian Solar Inc.',
              'pvsystem_id': 2214,
              'pvmodule_id': 1556,
              'num_of_modules': 16,
              'num_of_strings': 2,
              'smallest_string': 8,
              'largest_string': 8,
              'properties_updated_per_dev_id': 10072,
              'properties_id': 401,
              'device_id': 1556,
              'ullisting_refnum': 249143,
              'nameplaterating': 330,
              'cell_type': 'Polycrystalline',
              'total_number_cells': 144,
              'number_cell_inseries': 0,
              'number_cells_inparallel': 0,
              'pmp': 330,
              'power_tolerance': 5,
              'voc': 45.5,
              'isc': 9.19999980926514,
              'vmp': 38,
              'vnoct': 34.5999984741211,
              'vlow': 0,
              'imp': 8.6899995803833,
              'inoct': 7.03000020980835,
              'ilow': 0,
              'noct': 43,
              'max_series_fuse': 30,
              'max_system_v': 1500,
              'tc_voc_percent': -0.310000002384186,
              'tc_vpmax_percent': 0,
              'tc_isc_percent': 0.0500000007450581,
              'tc_ipmax_percent': 0,
              'tc_pmp_percent': -0.38999998569488503,
              'fire_class': 'C',
              'fire_type': 1,
              'max_unitsperbranch': 0,
              'max_power_output': 0,
              'max_accurrent_120': 0,
              'max_accurrent_208': 0,
              'max_accurrent_240': 0,
              'max_accurrent_277': 0,
              'max_accurrent_480': 0,
              'eff': 0,
              'min_temp': 0,
              'max_temp': 0,
              'user_category_code': 'Employee',
              'certificationlab_id': 1
            },
            'interconnection': {
              'bussbar_rating': 225,
              'supply_ocpd_rating': 200,
              'load_breaker_total': 50
            },
            'system': {
              'bussbar_rating': 225,
              'supply_ocpd_rating': 200,
              'load_breaker_total': 50
            },
            'array': {}
          }
        }
      },
      users: {},
    };
  }

  var init_state = {
    ui: {
      home_url: location.origin + location.pathname + '#/',
      page_id: ['default'],
      title: 'Solar Certification',
      menu: [
        'request',
        'account',
        'admin',
      ]
    },
    user_id: user_id,
    user: false,
    admin: {},
    db: db,
    pages: {
      express: mk_system_data(express_inputs),
      manual: mk_system_data(express_inputs),
      module: {
        inputs: module_inputs,
      }
    }
  };

  return init_state;
}
