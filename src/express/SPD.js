var spd_algorithm = require('spd_algorithm');

///////////////////////////////////////////
export default function(system){
  if( system ){
    var SPD_input = {
      data: system,
      server: {
        host: location.host
      },
      agency: 'Solar Plans Designer .com',
      note: 'This site and drawings are for educational purposes only.'
    };
    console.log(system);
    var system_settings = spd_algorithm(SPD_input);
    // console.log('system_settings',system_settings);

    var status = system_settings.state.notes.errors.length ? 'error' : 'pass';
    if( status === 'pass'){
      var svgs = system_settings.drawing.svgs.map(function(svg){
        return svg.outerHTML;
      });

      return {
        status: status,
        system_settings,
        notes: system_settings.state.notes,
        SVGs: svgs,
        data: system,
        state: system_settings.state.system,
      };
    } else if( status === 'error' ){
      return {
        status: status,
        system_settings,
        notes: system_settings.state.notes,
        SVGs: [],
        data: system,
        state: system_settings.state.system,
      };
    }

  } else {
    return {
      status: 'DB data not available',
      system_settings: false,
      notes: false,
      SVGs: false,
      data: false,
      state: false,
    };
  }

}
