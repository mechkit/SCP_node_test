import f from 'functions';

export default function (context) {

  var pages = {};

  context.keys().forEach(function (file_path) {

    // var page_name = file_path.split(/\/|\./).slice(-2)[0];
    // var title = f.pretty_name(page_name);
    // var location = file_path.split(/\/|\./).slice(2,-2);
    var page_id = file_path.split(/\/|\./).slice(2,-1).join('/');
    var mk_page_spec = context(file_path).default;

    pages[page_id] = (mk_page_spec);

  });

  return pages;

}
