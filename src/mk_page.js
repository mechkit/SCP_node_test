import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import titlebar_spec from './titlebar_spec';
import $ from 'specdom';
import require_pages from './require_pages';

var context = require.context('./page/', true, /\.js$/);
var pages = require_pages(context);

var content_specdom = $('#content');

export default function(state, actions){


  var page = pages[state.ui.page_id];
  var page_content_specs;
  if( ! page ){
    page_content_specs = pages[404](state);
  } else {
    page_content_specs = page(state,actions);
  }

  // TODO: replace with secure access
  if( ! state.user ){
    state.ui.menu = [];
  } else if( state.user.account_type === 'admin' ){
    state.ui.menu = [
      'request',
      'account',
      'admin',
    ];
  } else {
    state.ui.menu = [
      'request',
      'account',
    ];
  }

  // var titlebar_content = titlebar_spec('Solar Plans Designer', ['about'], state.ui.page_id);
  var titlebar_content = titlebar_spec(state);

  var specs = {
    tag: 'div',
    children: [
      {
        tag: 'div',
        props: {
          id: 'titlebar'
        },
        children: [titlebar_content]
      },
      div({class:'transition'}),
      div({id:'background'},[
        div({class:'display_area'},[
          page_content_specs
        ])
      ])
    ]
  };
  // content_specdom.load(specs);
  content_specdom.clear().append(specs);
}
