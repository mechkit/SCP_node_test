import system_components from './system_components';

export default function(inputs){
  if( inputs === undefined || ( inputs.constructor === Object && Object.keys(inputs).length === 0 ) ){
    inputs = {
      inverter: {},
      optimizer: {},
      module: {},
      interconnection: {},
      storage: {},
    };
  }

  var inverter_data = {};
  if( inputs.inverter && inputs.inverter.manufacturer_name && inputs.inverter.device_name ){
    var inverter_spec_id = system_components.inverter_tree[inputs.inverter.manufacturer_name][inputs.inverter.device_name];
    var inverter_values = system_components.inverter[inverter_spec_id];
    if(inverter_values){
      system_components.inverter_keys.forEach(function(value_name,i){
        // console.log(value_name, inverter_values[i]);
        var inverter_value = inverter_values[i] === undefined ? '' : inverter_values[i];
        inverter_data[value_name.toLowerCase()] = inverter_value;
      });
      if( inverter_data.device_type_id === 53 ){
        inverter_data.system_type = 'string';
      } else if(inverter_data.device_type_id === 54){
        inverter_data.system_type = 'micro';
      } else if(inverter_data.device_type_id === 56){
        inverter_data.system_type = 'optimizer';
      } else {
        console.warn('System type unknown');
      }
    }
  } else {
    system_components.inverter_keys.forEach(function(value_name){
      inverter_data[value_name.toLowerCase()] = false;
    });
  }
  // var optimizer_data = {};



  inputs.optimizer.manufacturer_name = 'SolarEdge';
  var optimizer_data = {};
  if( inputs.optimizer && inputs.optimizer.manufacturer_name && inputs.optimizer.device_name ){
    var optimizer_spec_id = system_components.optimizer_tree[inputs.optimizer.manufacturer_name][inputs.optimizer.device_name];
  }
  var optimizer_values = system_components.optimizer[optimizer_spec_id];
  system_components.optimizer_keys.forEach(function(value_name,i){
    value_name = value_name.toLowerCase();
    if( inverter_data.device_type_id !== 56 ){
      optimizer_data[value_name] = null;
    } else if( inputs.optimizer && inputs.optimizer.manufacturer_name && inputs.optimizer.device_name ){
      optimizer_data[value_name] = optimizer_values[i];
    } else {
      optimizer_data[value_name] = undefined;
    }
    inverter_data['opti_'+value_name] = optimizer_data[value_name];
  });

  var module_data = {};
  if( inputs.module && inputs.module.manufacturer_name && inputs.module.device_name ){
    var module_spec_id = system_components.module_tree[inputs.module.manufacturer_name][inputs.module.device_name];
    var module_values = system_components.module[module_spec_id];
    if(module_values){
      system_components.module_keys.forEach(function(value_name,i){
        var module_value = module_values[i] === undefined ? '' : module_values[i];
        module_data[value_name.toLowerCase()] = module_value;
      });
    }
  }


  var system = {
    'company': {
      'device_id': null,
      'certification_id': null,
      'old_certification_number': null,
      'company_id': null,
      'name': null,
      'line_1': null,
      'line_2': null,
      'line_3': null,
      'city': null,
      'state': null,
      'zipcode': null,
      'country': null,
      'primary_phone_number': null,
      'web_site': null,
      'email': null
    },
    inverter: Object.assign({},inverter_data,inputs.inverter),
    // optimizer: Object.assign({},optimizer_data,inputs.optimizer),
    optimizer: Object.assign({},optimizer_data,inputs.optimizer),
    module: Object.assign({},module_data,inputs.module),
    interconnection: Object.assign({},inputs.interconnection),
    system: inputs.interconnection || {},
    array: {},
  };

  system.inverter['grid_voltage'] = 240;
  //system.interconnection['grid_voltage'] = system.inverter['grid_voltage'];

  // system.module['num_of_modules'] = inputs.module['num_of_modules'];
  // system.module['num_of_strings'] = inputs.module['num_of_strings'];
  // system.module['smallest_string'] = inputs.module['smallest_string'];
  // system.module['largest_string'] = inputs.module['largest_string'];
  return {
    inputs,
    system,
  };
}
