import inverter from './data/data_inverter.json';
import inverter_keys from './data/data_inverter_keys.js';
import inverter_tree from './data/data_inverter_tree.js';
import module from './data/data_module.json';
import module_keys from './data/data_module_keys.js';
import module_tree from './data/data_module_tree.js';
import optimizer from './data/data_optimizer.json';
import optimizer_keys from './data/data_optimizer_keys.js';
import optimizer_tree from './data/data_optimizer_tree.js';

export default {
  inverter,
  inverter_keys,
  inverter_tree,
  module,
  module_keys,
  module_tree,
  optimizer,
  optimizer_keys,
  optimizer_tree,
};
