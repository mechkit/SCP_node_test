import f from 'functions';
import reducer_express from './reducer/express';
import mk_system_data from './mk_system_data';
import SPD from './express/SPD';
import Chance from 'chance';

var chance = new Chance('SPD');

export default {

  init: function(state,action){
    console.log('reducer: init');
    return state;
  },
  add_user: function(state,action){
    var user_details = action.arguments[0];
    var user_id = chance.hash({length: 4});
    user_details.user_id = user_id;
    // user_details.account_type = 'installer';
    user_details.system_ids = user_details.system_ids || [];
    while( state.db.users[user_id] ){
      user_id = chance.hash({length: 4});
    }
    state.db.users[user_id] = user_details;
    return state;
  },
  set_user_privilege: function(state,action){
    var user_id = action.arguments[0];
    var account_type = action.arguments[1];
    state.admin[user_id].account_type = account_type;
    return state;
  },
  login: function(state,action){
    var user_id = action.arguments[0];

    if(user_id){
      state.user = state.db.users[user_id];
      console.log('logged in: '+state.user.name);
    } else if( user_id === null ){
      console.log('...loging out: '+state.user.name);
      state.user = null;
    }

    sessionStorage.setItem('user_id', user_id);

    return state;
  },
  set_page: function(state,action){
    var page_id = action.arguments[0];

    state.ui.page_id = page_id;

    return state;
  },

  update_inputs: function(state,action){

    state = reducer_express(state,action);

    var id = action.arguments[0];
    var value = action.arguments[1];
    var [page,section,name] = id.split('.');
    state.pages[page].inputs[section] = state.pages[page].inputs[section] || {};
    // state.pages[page].inputs = {};
    value = f.str_to_num(value);
    if(value==='') value = undefined;
    state.pages[page].inputs[section][name] = value;

    // console.log(state.pages[page].inputs);
    if( name === 'manufacturer_name' ){
      state.pages[page].inputs[section]['device_name'] = undefined;
    }

    sessionStorage.setItem('express_inputs', JSON.stringify(state.pages.express.inputs));
    state.pages[page] = mk_system_data(state.pages[page].inputs);
    return state;
  },

  submit: function(state,action){
    var page_name = action.arguments[0];
    var results = SPD(state.pages[page_name].system);
    var system_settings = results.system_settings;
    var status = system_settings.state.notes.errors.length ? 'error' : 'pass';

    if( status === 'pass' ){
      var system_id = chance.hash({length: 5});
      while( state.db.systems[system_id] ){
        system_id = chance.hash({length: 5});
      }
      var system = {
        user: state.user_id,
        inputs: state.pages[page_name].inputs,
        system: state.pages[page_name].system,
      };
      // state.db.systems[system_id] = system;
      // state.db.users[user_id].system_ids.push(system_id);
      // state.pages[page_name] = mk_system_data();
    }

    console.log(state.db.systems);
    return state;
  },

  clear_inputs: function(state,action){
    var page_name = action.arguments[0];
    state.pages[page_name] = mk_system_data();
    return state;
  }


};
