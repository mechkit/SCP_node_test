#!/usr/bin/env node

var fs = require('fs');
var f = require('functions');
var path = require('path');
var local_path = __dirname;

fs.readdirSync('./').forEach(function(file_name){
  var data = {
    // manufacturer_model: [],
    tree: {},
    keys: [],
    entries: [],
  };
  var ext = file_name.split('.').slice(-1)[0];
  var type = file_name.split('.').slice(-2,-1)[0];
  if( ext === 'json' && type === 'source' ){
    var json_data = JSON.parse( fs.readFileSync( './' + file_name ) );
    console.log('File:', file_name);
    data.keys = Object.keys( json_data[0] );
    json_data.forEach(function(entry,i){
      var row_array = [];
      data.keys.forEach(function(key){
        row_array.push( f.str_to_num(entry[key]) );
      });
      data.entries.push(row_array);
      // data.manufacturer_model.push( entry['MANUFACTURER_NAME'] + ' ' + entry['DEVICE_NAME'] );
      data.tree[entry['MANUFACTURER_NAME']] = data.tree[entry['MANUFACTURER_NAME']] || {};
      data.tree[entry['MANUFACTURER_NAME']][entry['DEVICE_NAME']] = i;
    });

    var name = file_name.split('.').slice(0,-2).join('.');

    var tree_file_path = path.join(local_path, name +  '_tree.js');
    fs.writeFileSync(tree_file_path, 'export default ' + JSON.stringify(data.tree), {encoding: 'utf8'});
    var keys_file_path = path.join(local_path, name +  '_keys.js');
    fs.writeFileSync(keys_file_path, 'export default ' + JSON.stringify(data.keys), {encoding: 'utf8'});
    var entries_file_path = path.join(local_path, name +  '.json');
    fs.writeFileSync(entries_file_path, JSON.stringify(data.entries), {encoding: 'utf8'});

  }
});
