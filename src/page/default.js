import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';


export default function(state, actions){
  var login_specs = div([]);
  if( state.user ){
    login_specs.children = login_specs.children.concat([
      span('Welcome: '+state.user.name),
      br(),
      a('logout',state.ui.home_url,{
        onclick: function(e){
          actions.login(null);
        }
      })

    ]);
  } else {
    var users_ids = Object.keys(state.db.users);
    var user_options = [
      option(' ')
    ].concat(
      users_ids.map(user_id=>{
        return option({id:user_id},state.db.users[user_id].name);
      })
    );
    login_specs.children = login_specs.children.concat([
      span('login: '),
      select(
        {
          id: 'login',
          onchange: function(e){
            var user_id = e.target.options[e.target.selectedIndex].id;
            actions.login(user_id);
          }
        },
        user_options
      )
    ]);
  }

  var specs = div({class:'page'}, [
    p('FSEC\'s PV System Certification and Module Registration processes help insure the safety and quality of solar installations in Florida and provide an assurance to installers, consumers, and financiers that approved designs meet accepted codes and industry practices. FSEC offers both an express system certification service to certify, and generate code compliant three line drawings suitable for permitting simple PV systems and a manual system certification service to have solar professionals review existing documentation.'),
    br(),
    p('The Laws of Florida (§ 377.705 FS) require that all solar equipment manufactured or sold in the state of Florida comply with solar equipment standards developed and promulgated by the Florida Solar Energy Center (FSEC).'),
    br(),
    login_specs,
    br(),
    'try our ',
    a('express',state.ui.home_url + 'express'),
    ' design tool.'
  ]);



  return specs;
}
