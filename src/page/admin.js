import $ from 'specdom';
import f from 'functions';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';



export default function(state, actions){

  var user_list_specs = table([]);
  user_list_specs.children.push(tr([
    th('user_id'),
    th('name'),
    th('company'),
    th('account type'),
    th(),
  ]));
  for( var user_id in state.db.users ){
    var user = state.db.users[user_id];
    user_list_specs.children.push(tr([
      td(user_id),
      td(user.name),
      td(user.company),
      td(user.account_type),
      // td([a('edit',state.ui.home_url + 'account')]),
      td('edit'),
    ]));
  }


  var system_list_specs = table([]);
  system_list_specs.children.push(tr([
    th('system id'),
    th('user id'),
    th('manufacturer name'),
    th('device name'),
    th('manufacturer name'),
    th('device name'),
    // th('edit'),
  ]));
  for( var system_id in state.db.systems ){
    var system = state.db.systems[system_id];
    console.log(system);
    system_list_specs.children.push(tr([
      td(system_id),
      td(system.user_id),
      td(system.inputs.inverter.manufacturer_name),
      td(system.inputs.inverter.device_name),
      td(system.inputs.module.manufacturer_name),
      td(system.inputs.module.device_name),
      // td([a('edit',state.ui.home_url + 'account')]),
      td('edit'),
    ]));
  }

  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'Users'),
      div({class:'page'},[user_list_specs]),
      span({class:'page_title'},'Systems'),
      div({class:'page'},[system_list_specs]),
    ])
  ]);

  return page_spec;
}
