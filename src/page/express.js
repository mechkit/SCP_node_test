import $ from 'specdom';
import f from 'functions';
import system_components from '../system_components';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';
// import base64url from 'base64-url';
import SPD  from '../express/SPD';
import mk_page from '../mk_page';


export default function(state, actions){
  var system = state.pages.express.system;
  var inputs = state.pages.express.inputs;

  var svgs;

  var inverter_manufacturers = [''].concat(Object.keys(system_components.inverter_tree));
  // var inverter_manufacturer_input = document.getElementById('inverter.manufacturer') || false;
  var inverter_models = system.inverter && system.inverter.manufacturer_name ? [''].concat(Object.keys(system_components.inverter_tree[system.inverter.manufacturer_name])) : [];
  // var optimizer_manufacturers = [''].concat(Object.keys(system_components.optimizer_tree));
  // var optimizer_manufacturer_input = document.getElementById('optimizer.manufacturer') || false;
  // system.optimizer.manufacturer_name = 'SolarEdge';
  var optimizer_manufacturer = ['SolarEdge'];
  var optimizer_models = system.optimizer && system.optimizer.manufacturer_name ? [''].concat(Object.keys(system_components.optimizer_tree[system.optimizer.manufacturer_name])) : [];
  var module_manufacturers = [''].concat(Object.keys(system_components.module_tree));
  // var module_manufacturer_value = document.getElementById('module.manufacturer') || false;
  var module_models = system.module && system.module.manufacturer_name ? [''].concat(Object.keys(system_components.module_tree[system.module.manufacturer_name])) : [];

  var option_lists = {
    'module.manufacturer_name': module_manufacturers,
    'module.device_name': module_models,
    'inverter.manufacturer_name': inverter_manufacturers,
    'inverter.device_name': inverter_models,
    'optimizer.manufacturer_name': optimizer_manufacturer,
    'optimizer.device_name': optimizer_models,
  };


  // console.log('state',state);
  if(system.inverter.system_type === 'optimizer'){
    var optimizer_device_name_disabled = null;
  } else {
    var optimizer_device_name_disabled = true;
  }


  var results = SPD(system);
  var system_settings = results.system_settings;
  var status = system_settings.state.notes.errors.length ? 'error' : 'pass';
  var svg_specs = [];
  if( status === 'pass'){
    console.log('System passes');
    svgs = system_settings.drawing.svgs.map(function(svg){
      return svg.outerHTML;
    });
    svg_specs = svgs.map(svg => span({innerHTML:svg}));
  } else {
    console.log('errors:' ,system_settings.state.notes);
  }

  if(system_settings.state.notes){
    var notes_specs = [];

    if(system_settings.state.notes.errors.length){
      notes_specs.push(span('Errors:'));
      notes_specs.push(ul(system_settings.state.notes.errors.map(text => li(text))));
    }
    if(system_settings.state.notes.warnings.length){
      notes_specs.push(span('Warnings:'));
      notes_specs.push(ul(system_settings.state.notes.warnings.map(text => li(text))));
    }
    if(system_settings.state.notes.info.length){
      notes_specs.push(span('Info:'));
      notes_specs.push(ul(system_settings.state.notes.info.map(text => li(text))));
    }

    // var specdom_notes = $('#notes');
    // specdom_notes.clear();
    // specdom_notes.append( notes_specs );
  }
  // var specdom_svgs = $('#svgs');
  // if(svgs){
  //   var svg_specs = svgs.map(svg => span({innerHTML:svg}));
  //   specdom_svgs.clear();
  //   specdom_svgs.append( svg_specs );
  // } else {
  //   specdom_svgs.clear();
  // }



  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'System configuration'),
      div({class:'page'},[

        div({class:'section_title'},'- PV array -'),
        div(['Choose the PV module the system uses and indicate the combined number of strings on all inverters, the number of modules in the smallest string(s), and the number of modules in the largest string(s), and the total number of modules for that model among all inverters. For systems using microinverters or ACPV modules, please provide information for branch circuits rather than series strings. If the module is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist(actions,
            'input_list_wide',
            'express.module.manufacturer_name',
            option_lists['module.manufacturer_name'],
            'Manufacturer',
            inputs.module.manufacturer_name
          ),
          mklist(actions,
            'input_list_mid',
            'express.module.device_name',
            option_lists['module.device_name'],
            'Model',
            inputs.module.device_name
          ),
          // span({class:'input_group'},[
          //   span({class:''},'Module Info'),
          //   span({class:'input_button'},'Module Info'),
          // ]),
          mkinput(actions,
            'input_int',
            'express.module.num_of_strings',
            '# of Strings',
            inputs.module.num_of_strings
          ),
          mkinput(actions,
            'input_int',
            'express.module.smallest_string',
            'Smallest String',
            inputs.module.smallest_string
          ),
          mkinput(actions,
            'input_int',
            'express.module.largest_string',
            'Largest String',
            inputs.module.largest_string
          ),
          mkinput(actions,
            'input_int',
            'express.module.num_of_modules',
            '# of Modules',
            inputs.module.num_of_modules
          ),
          mkinput(actions,
            'input_int',
            'express.module.array_offset_from_roof',
            'PV Module\'s distance above roof (in)',
            inputs.module.array_offset_from_roof
          ),
        ]),

        div({class:'section_title'},'- Power Conditioning -'),
        div(['Select the inverter model the system uses, the quantity of that inverter being used, and the operating voltage. If the inverter is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist(actions,
            'input_list_narrow',
            'express.inverter.manufacturer_name',
            option_lists['inverter.manufacturer_name'],
            'Manufacturer',
            inputs.inverter.manufacturer_name
          ),
          mklist(actions,
            'input_list_wide',
            'express.inverter.device_name',
            option_lists['inverter.device_name'],
            'Model',
            inputs.inverter.device_name
          ),
          mklist(actions,
            'input_list_narrow',
            'express.optimizer.manufacturer_name',
            option_lists['optimizer.manufacturer_name'],
            'Manufacturer',
            inputs.inverter.device_name,
            {hidden:true}
          ),
          mklist(actions,
            'input_list_narrow',
            'express.optimizer.device_name',
            option_lists['optimizer.device_name'],
            'Optimizer Model',
            inputs.inverter.device_name,
            {disabled:optimizer_device_name_disabled}
          ),
        ]),

        div({class:'section_title'},'- Interconnection -'),
        div({'class':'input_line'},[
          mkinput(actions,
            'input_int',
            'express.interconnection.bussbar_rating',
            'Busbar Rating (A)',
            inputs.interconnection.bussbar_rating
          ),
          mkinput(actions,
            'input_int',
            'express.interconnection.supply_ocpd_rating',
            'Supply OCPD Rating (A)',
            inputs.interconnection.supply_ocpd_rating
          ),
          mkinput(actions,
            'input_int',
            'express.interconnection.load_breaker_total',
            'Total Of Load Breakers (A)',
            inputs.interconnection.load_breaker_total
          ),
        ]),
        div({class:'section_title'},' '),
        div({'class':'input_line'},[
          div({class:'question_black'},[
            span('clear inputs?', {class:'',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',null);
              });
            }})
          ]),
          span('really?',{hidden:'true',class:'confirm_clear question_black'}),
          div({class:'confirm_clear',hidden:'true'},[
            span('no',{class:'question_green',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
            }}),
          ]),
          div({class:'confirm_clear',hidden:'true'},[
            span('yes',{class:'question_red',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
              actions.clear_inputs('express');
            }}),
          ]),
        ]),
      ]),

      span({class:'page_title'},'Drawing'),
      div({class:'page'},[
        div({id:'notes',class:'notes'},notes_specs),
        div({id:'svgs',class:'svgs'},svg_specs),
      ]),
      br(),
      button('submit',{
        id:'submit',
        class:'button_button',
        onclick: function(){
          console.log('submit');
          actions.submit('express');
        }
      }),
    ]),
  ]);


  return page_spec;
}
