import $ from 'specdom';
import f from 'functions';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';

export default function(state, actions){
  var system = state.pages.module.system;
  var inputs = state.pages.module.inputs;

  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'System configuration'),
      div({class:'page'},[

        div({class:'section_title'},'- PV array -'),
        div(['Choose the PV module the system uses and indicate the combined number of strings on all inverters, the number of modules in the smallest string(s), and the number of modules in the largest string(s), and the total number of modules for that model among all inverters. For systems using microinverters or ACPV modules, please provide information for branch circuits rather than series strings. If the module is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        table({class:'module_table'},[
          tr([
            td([span('PV Module Type')]),
            td([select(
              {
                class: 'input_list_wide',
                id: 'module.type.type',
              },['','AC','DC'].map(value=>{
                var selected = null;
                if( value === inputs.type.type ) select = true;
                return option({selected:selected},value);
              })
            )]),
          ]),
          //*
          tr([
            td([span('Manufacturer Name')]),
            td([input({
              class: 'input_int',
              id: 'module.type.manufacturer_name',
              value: inputs.type.manufacturer_name,
            })]),
          ]),
          tr([
            td([span('Model Name')]),
            td([input({
              class: 'input_int',
              id: 'module.type.model_name',
              value: inputs.type.model_name,
            })]),
          ]),
          tr([
            td([span('Nameplate Rating (Wp @25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.nameplate_rating',
              value: inputs.type.nameplate_rating,
            })]),
          ]),
          tr([
            td([span('Cell Type')]),
            td([input({
              class: 'input_int',
              id: 'module.type.cell_type',
              value: inputs.type.cell_type,
            })]),
          ]),
          tr([
            td([span('Total Number of Cells')]),
            td([input({
              class: 'input_int',
              id: 'module.type.total_number_of_cells',
              value: inputs.type.total_number_of_cells,
            })]),
          ]),
          tr([
            td([span('Max Power @ STC (Pmp) (Wp @25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.max_power_stc',
              value: inputs.type.max_power_stc,
            })]),
          ]),
          tr([
            td([span('Power Tolerance (-/+ W)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.power_tolerance',
              value: inputs.type.power_tolerance,
            })]),
          ]),
          tr([
            td([span('Open Circuit Voltage @ STC(Voc) (V @25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.open_circuit_voltage_stc',
              value: inputs.type.open_circuit_voltage_stc,
            })]),
          ]),
          tr([
            td([span('Short Circuit Current @ STC(Isc) (A @25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.short_circuit_current_stc',
              value: inputs.type.short_circuit_current_stc,
            })]),
          ]),
          tr([
            td([span('Voltage Max Power @ STC (Vmp) (V @25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.voltage_max_power_stc',
              value: inputs.type.voltage_max_power_stc,
            })]),
          ]),
          tr([
            td([span('Nominal Operating Cell Temperature (°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.nominal_operating_cell_temperature',
              value: inputs.type.nominal_operating_cell_temperature,
            })]),
          ]),
          tr([
            td([span('Voltage @ Max Power & NOCT (NOCT and 800 w/m²) (V)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.voltage_max_power_noct',
              value: inputs.type.voltage_max_power_noct,
            })]),
          ]),
          tr([
            td([span('Current Max Power @ STC (Imp) (A(25°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.current_max_power_stc',
              value: inputs.type.current_max_power_stc,
            })]),
          ]),
          tr([
            td([span('Current @ Max Power & NOCT (NOCT and 800 w/m²) (A)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.current_max_power_noct',
              value: inputs.type.current_max_power_noct,
            })]),
          ]),
          tr([
            td([span('Maximum overcurrent device rating (A)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.maximum_overcurrent_device_rating',
              value: inputs.type.maximum_overcurrent_device_rating,
            })]),
          ]),
          tr([
            td([span('Maximum system voltage rating (V)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.maximum_system_voltage_rating',
              value: inputs.type.maximum_system_voltage_rating,
            })]),
          ]),
          tr([
            td([span('Temperature Coefficient for Voc (ß) (%/°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.temperature_coefficient_for_voc',
              value: inputs.type.temperature_coefficient_for_voc,
            })]),
          ]),
          tr([
            td([span('Temp Coefficient of Isc (αIsc) (%/°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.temp_coefficient_of_isc',
              value: inputs.type.temp_coefficient_of_isc,
            })]),
          ]),
          tr([
            td([span('Temperature Coefficient of Pmax(γ) (%/°C)')]),
            td([input({
              class: 'input_int',
              id: 'module.type.temperature_coefficient_of_pmax',
              value: inputs.type.temperature_coefficient_of_pmax,
            })]),
          ]),
          tr([
            td([span('Fire class')]),
            td([input({
              class: 'input_int',
              id: 'module.type.fire_class',
              value: inputs.type.fire_class,
            })]),
          ]),
          tr([
            td([span('Fire type')]),
            td([input({
              class: 'input_int',
              id: 'module.type.fire_type',
              value: inputs.type.fire_type,
            })]),
          ]),
          tr([
            td([span('Certification lab for UL 1703 listing')]),
            td([select(
              {
                class: 'input_int',
                id: 'module.type.certification_lab_for_ul_1703_listing',
              },['','CSA','Intertek','TÜV','UL'].map(value=>{
                var selected = null;
                if( value === inputs.type.certification_lab_for_ul_1703_listing ) select = true;
                return option({selected:selected},value);
              })
            )]),
          ]),
          tr([
            td([span('UL Listing Reference Number')]),
            td([input({
              class: 'input_int',
              id: 'module.type.ul_listing_reference_number',
              value: inputs.type.ul_listing_reference_number,
            })]),
          ]),
          //*/
        ])
      ]),
    ])
  ]);

  return page_spec;
}
