import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';

export default function(state){
  var specs = div({class:'page'}, [
    'Unknown link. ',
    br(),
    'Return ',
    a('home',state.ui.home_url)
  ]);

  return specs;
}
