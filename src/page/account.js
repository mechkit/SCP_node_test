import $ from 'specdom';
import f from 'functions';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';



export default function(state, actions){

  var sheet_content = table([]);
  sheet_content.children.push(tr([
    td('key'),
    td('value'),
  ]));

  for( var key in state.user ){
    sheet_content.children.push(tr([
      td(key),
      td(state.user[key]),
    ]));
  }

  console.log(state.user);
  var system_list_specs = span();
  if( state.user.system_ids.length ){
    system_list_specs = table([]);
    system_list_specs.children.push(tr([
      th('system id'),
      th('user id'),
      th('manufacturer name'),
      th('device name'),
      th('manufacturer name'),
      th('device name'),
      // th('edit'),
    ]));
    console.log(state.user.system_ids);
    state.user.system_ids.forEach(function(system_id){
      var system = state.db.systems[system_id];
      console.log(system_id,state.db.systems);
      system_list_specs.children.push(tr([
        td(system_id),
        td(system.user_id),
        td(system.inputs.inverter.manufacturer_name),
        td(system.inputs.inverter.device_name),
        td(system.inputs.module.manufacturer_name),
        td(system.inputs.module.device_name),
        // td([a('edit',state.ui.home_url + 'account')]),
        td('edit'),
      ]));
    });
  }

  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'Account configuration'),
      div({class:'page'},[
        sheet_content,
        span('edit'),
        system_list_specs,
      ]),
    ])
  ]);

  return page_spec;
}
