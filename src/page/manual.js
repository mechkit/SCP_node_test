import $ from 'specdom';
import f from 'functions';
import system_components from '../system_components';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';

export default function(state, actions){
  var system = state.pages.manual.system;
  var inputs = state.pages.manual.inputs;

  var svgs;

  var inverter_manufacturers = [''].concat(Object.keys(system_components.inverter_tree));
  // var inverter_manufacturer_input = document.getElementById('inverter.manufacturer') || false;
  var inverter_models = system.inverter && system.inverter.manufacturer_name ? [''].concat(Object.keys(system_components.inverter_tree[system.inverter.manufacturer_name])) : [];
  // var optimizer_manufacturers = [''].concat(Object.keys(system_components.optimizer_tree));
  // var optimizer_manufacturer_input = document.getElementById('optimizer.manufacturer') || false;
  // system.optimizer.manufacturer_name = 'SolarEdge';
  var optimizer_manufacturer = ['SolarEdge'];
  var optimizer_models = system.optimizer && system.optimizer.manufacturer_name ? [''].concat(Object.keys(system_components.optimizer_tree[system.optimizer.manufacturer_name])) : [];
  var module_manufacturers = [''].concat(Object.keys(system_components.module_tree));
  // var module_manufacturer_value = document.getElementById('module.manufacturer') || false;
  var module_models = system.module && system.module.manufacturer_name ? [''].concat(Object.keys(system_components.module_tree[system.module.manufacturer_name])) : [];

  var charge_controllers = [
    '',
    'Magnum - Energy	MS4024PAE',
    'Magnum - Energy	MS4448 PAE',
    'Outback Power Systems - FM60',
    'Outback Power Systems - FM80',
    'SMA - SI4548US',
    'SMA - SI5048US',
    'SMA - SI6048US',
    'Schneider - MPPT 80 600',
    'SolarEdge - SE7600A-USS',
    'Xantrex - XW-MPPT60-150',
    'Xantrex - XW-MPPT80-600',
    'sonnenBatterie - eco 16',
  ];

  var storage_batterys = [
    '',
    'Apex - APX6-205',
    'Aquion Energy - Aspen 48S-2.2',
    'CES - PG11',
    'Concorde - PVX-12150HT',
    'Concorde - PVX-2120L',
    'Concorde - PVX-2580',
    'Concorde - PVX-4050HT',
    'Crown - CR430',
    'Deka Solar - 8A31',
    'Deka Solar - 8AGC2',
    'Deka Solar - 8G4D',
    'East Penn Manufacturing - EGC2',
    'Full River - DC335-6',
    'GNB Industrial Power - Absolyte GP 1-100G51',
    'MK Battery - 8A31DT',
    'MK Battery - 8A8D',
    'MK Battery - 8A8DDT',
    'Optimized Energy Storage - OES3-48V',
    'Outback Power Systems - EnergyCell 170RE',
    'Outback Power Systems - EnergyCell 200RE',
    'Rolls - S2-3560AGM',
    'Rolls - S6-460AGM',
    'Surrette - S-530',
    'Trojan Battery Company - L16RE-2V',
    'UPG - UB63800',
    'UPG - UBGC2',
  ];

  var option_lists = {
    'module.manufacturer_name': module_manufacturers,
    'module.device_name': module_models,
    'inverter.manufacturer_name': inverter_manufacturers,
    'inverter.device_name': inverter_models,
    'storage.charge_controller': charge_controllers,
    'storage.battery': storage_batterys,
    'optimizer.manufacturer_name': optimizer_manufacturer,
    'optimizer.device_name': optimizer_models,
  };


  // console.log('state',state);
  var optimizer_device_name_disabled;
  if(system.inverter.system_type === 'optimizer'){
    optimizer_device_name_disabled = null;
  } else {
    optimizer_device_name_disabled = true;
  }





  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'System configuration'),
      div({class:'page'},[

        div({class:'section_title'},'- PV array -'),
        div(['Choose the PV module the system uses and indicate the combined number of strings on all inverters, the number of modules in the smallest string(s), and the number of modules in the largest string(s), and the total number of modules for that model among all inverters. For systems using microinverters or ACPV modules, please provide information for branch circuits rather than series strings. If the module is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist(actions,
            'input_list_wide',
            'manual.module.manufacturer_name',
            option_lists['module.manufacturer_name'],
            'Manufacturer',
            inputs.module.manufacturer_name
          ),
          mklist(actions,
            'input_list_mid',
            'manual.module.device_name',
            option_lists['module.device_name'],
            'Model',
            inputs.module.device_name
          ),
          // span({class:'input_group'},[
          //   span({class:''},'Module Info'),
          //   span({class:'input_button'},'Module Info'),
          // ]),
          mkinput(actions,
            'input_int',
            'manual.module.num_of_strings',
            '# of Strings',
            inputs.module.num_of_strings
          ),
          mkinput(actions,
            'input_int',
            'manual.module.smallest_string',
            'Smallest String',
            inputs.module.smallest_string
          ),
          mkinput(actions,
            'input_int',
            'manual.module.largest_string',
            'Largest String',
            inputs.module.largest_string
          ),
          mkinput(actions,
            'input_int',
            'manual.module.num_of_modules',
            '# of Modules',
            inputs.module.num_of_modules
          ),
          mkinput(actions,
            'input_int',
            'manual.module.array_offset_from_roof',
            'PV Module\'s distance above roof (in)',
            inputs.module.array_offset_from_roof
          ),
        ]),

        div({class:'section_title'},'- Power Conditioning -'),
        div(['Select the inverter model the system uses, the quantity of that inverter being used, and the operating voltage. If the inverter is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist(actions,
            'input_list_narrow',
            'manual.inverter.manufacturer_name',
            option_lists['inverter.manufacturer_name'],
            'Manufacturer',
            inputs.inverter.manufacturer_name
          ),
          mklist(actions,
            'input_list_wide',
            'manual.inverter.device_name',
            option_lists['inverter.device_name'],
            'Model',
            inputs.inverter.device_name
          ),
          mklist(actions,
            'input_list_narrow',
            'manual.optimizer.manufacturer_name',
            option_lists['optimizer.manufacturer_name'],
            'Manufacturer',
            inputs.inverter.device_name,
            {hidden:true}
          ),
          mklist(actions,
            'input_list_narrow',
            'manual.optimizer.device_name',
            option_lists['optimizer.device_name'],
            'Optimizer Model',
            inputs.inverter.device_name,
            {disabled:optimizer_device_name_disabled}
          ),
        ]),

        div({class:'section_title'},'- Storage -'),
        div({'class':'input_line'},[
          mklist(actions,
            'input_int',
            'manual.storage.charge_controller',
            option_lists['storage.charge_controller'],
            'Charge Controller',
            inputs.storage.charge_controller
          ),
          mklist(actions,
            'input_int',
            'manual.storage.battery',
            option_lists['storage.battery'],
            'Battery',
            inputs.storage.battery
          ),
          mkinput(actions,
            'input_int',
            'manual.storage.num_of_series_strings',
            '# of Series Strings',
            inputs.storage.num_of_series_strings
          ),
          mkinput(actions,
            'input_int',
            'manual.storage.num_of_batteries_per_series_string',
            '# of Batteries per Series String',
            inputs.storage.num_of_batteries_per_series_string
          ),
          mkinput(actions,
            'input_int',
            'manual.storage.total_num_of_batteries',
            'Total # of Batteries',
            inputs.storage.total_num_of_batteries
          ),
        ]),

        div({class:'section_title'},'- Interconnection -'),
        div({'class':'input_line'},[
          mkinput(actions,
            'input_int',
            'manual.interconnection.bussbar_rating',
            'Busbar Rating (A)',
            inputs.interconnection.bussbar_rating
          ),
          mkinput(actions,
            'input_int',
            'manual.interconnection.supply_ocpd_rating',
            'Supply OCPD Rating (A)',
            inputs.interconnection.supply_ocpd_rating
          ),
          mkinput(actions,
            'input_int',
            'manual.interconnection.load_breaker_total',
            'Total Of Load Breakers (A)',
            inputs.interconnection.load_breaker_total
          ),
        ]),

        div({class:'section_title'},' '),
        div({'class':'input_line'},[
          div({class:'question_black'},[
            span('clear inputs?', {class:'',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',null);
              });
            }})
          ]),
          span('really?',{hidden:'true',class:'confirm_clear question_black'}),
          div({class:'confirm_clear',hidden:'true'},[
            span('no',{class:'question_green',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
            }}),
          ]),
          div({class:'confirm_clear',hidden:'true'},[
            span('yes',{class:'question_red',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
              actions.clear_inputs('manual');
            }}),
          ]),
        ]),
      ]),

    ]),
  ]);


  return page_spec;
}
