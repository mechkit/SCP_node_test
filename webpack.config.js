var path = require('path');
var webpack = require('webpack');
//var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/app.js'],
  },
  output: {
    //path: path.resolve(__dirname, 'public'),
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: '[name].js'
  },

  //plugins: [
  //  new webpack.DefinePlugin({
  //    'process.env.NODE_ENV': JSON.stringify('dev')
  //  })
  //],
  devtool: 'source-map',

  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 4444
  },
  watchOptions: {
    ignored: [
      /node_modules([\\]+|\/)+(?!markdown_loader)/,
      /markdown_loader([\\]+|\/)node_modules/
    ]
  },
  plugins: [
    new webpack.IgnorePlugin(/jsdom/),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: /client/,
        //exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.md$/,
        include: /docs/,
        //exclude: /node_modules/,
        use: 'markdown_loader'
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
      },
    ]

  }

};
